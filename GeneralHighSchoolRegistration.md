# API

## Password Authorization

E-Albania user credentials:

/*REMOVED*/

### Getting Access Token

-   Request Type: `POST`
-   URL: `/oauth/token`
-   Headers:

    ```
    Content-Type: application/json
    ```

-   Body params:

    ```json
    {
        "grant_type": "password",
        "client_id": "2",
        "client_secret": "WyDZKxCLg59y6p5W50O2MOqQQLhNu6BhoOySVGu4",
        "username": "e-albania@api.al",
        "password": "ApiEAlbania@2019",
        "scope": "*"
    }
    ```

::: tip Success Reponse :::

```json
{
    "token_type": "Bearer",
    "expires_in": 7775999,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI2NzJhOWU5OTRiMzYxY2JhMmM1N2FkOTJiYWYxOGM2ZDdiZmYxNGE4Y2VjZjg4ZTJhMGMwOTY3MWM4ZDc0MTkwNjEzMmQ0Y2RkMDU3NTVkIn0.eyJhdWQiOiIyIiwianRpIjoiMjY3MmE5ZTk5NGIzNjFjYmEyYzU3YWQ5MmJhZjE4YzZkN2JmZjE0YThjZWNmODhlMmEwYzA5NjcxYzhkNzQxOTA2MTMyZDRjZGQwNTc1NWQiLCJpYXQiOjE1NjMzNTA5NzMsIm5iZiI6MTU2MzM1MDk3MywiZXhwIjoxNTcxMTI2OTcyLCJzdWIiOiIyM2EwNGRlZC1kNmY0LTRjMDctOTM1Yy1lNzBkNzdiYjk3OGMiLCJzY29wZXMiOlsiKiJdfQ.qcw4ejDNJfBvIT3CzlZqfr38VmKMTyXT0nD2siDlWmlX2CPZ9xJmUxjDtZQgRGZkV09KnBxhLg_GHYqkHlbCzeT6yi0RJrAYXdxiEJgXsfV0nB8LL7W8sExklrw2dK4N3WBx_Kbu3vGgebdneJFaVAm6SGNE7IFSucPiUjDV9jIL3OmUgf4tdUWIF3uxcmQuHH9Zz9jHIVml1EVNtjqcub1GgKdgGTB5ZKFo2lHKj3Z5F8tBwT72i9tp9S1ewjnNxTZC5xuIs7PwXUUrFg_OyYXehH2N-O6wmRdPAic8Z4CGqOZY9aWp-Ikv-RjBIxLlauTlQkJf3BxfCvw67TYaO8-R7NPmgOVde558q7NiEhNSgS8LPEIwEaiApR0F81BO8OA0PBD8wgNQVU0PZw-JGJBDvItwWvKvCG1QSy6JYGHaCrqkgyRSkgyPcUR5CinxdYbzh8MnyujDcq7Un-IM-x6fQqbDN6Yf5QX7Gbqvspfjb-UPAzgF323-33aF6vFHGlPsiKsc1QE3Dxyap480Abs5sfvYzY-xEuq3wwo97DkHyScWuHzK_GM3R-aiMuMYZE5qDFVFxKARGoH5e15VG-mzzjeKKROZGeNERtMm8eAaxEtOZvDFWKFLu6kKDKa8Byf7eQWAZi8Gr1NYY36FlTwfV6SGji3KCLqY3BBk9a8",
    "refresh_token": "def50200a4db012cbd30347d977b5f45e6ce9dce544893709a164af396cd4fe9c955a06c2af1ccbe63d8e5e971ede1646a12b284ec9e7e930475bc5f2006e2705978acdaec82608fe224ab3fca5a984507fde5e0017b36857ee1d3655f2ffae30699cb15e7a4b702e1374b6c9908f9529ee6eadba0fef8b0b8e808cf18e886408c5a91713f54e0113e9cb4c596df325234c16000b8801330dae65e4ad081bda18be0bb708f86d248e073d6fa6189fa9b7b3b2ca259813647e93042094890d4e2ae53da19b584cb63eeecbeea86fe648ed32b369808cc8cb34f037ea17952e7653d66c56644541842ef9d812dad5d6d67967776a02549e1b2de4e96d0f5fdef86e5f619b9435c6a81650b79d27e3cfb62cefce4282858ef6ed14a1de28c2e34b31d12e983e239926b615fa4562be84aff27c80d7dcedba8fa8b13841f5467bae4670caa16506987b2d1374bf2801c3118a2d2cd876a38c6fa8287cd9b4cd56fff8af906f7ce3fadaa7e4c1f77c9e8fedab4674220a319b0e629e757ec1eb7af1022a4577807f2795ae9"
}
```

Each consecutive request should the include the `Authorization` header, using the bearer scheme:

```
   Authorization: Bearer the-access-token
```

Access tokens are valid for **_90 days_**. Once the token has expired, it can be refreshed by using the refresh token:

### Refreshing tokens

-   Request Type: `POST`
-   URL: `/oauth/token`
-   Headers:

    ```
    Content-Type: application/json
    ```

-   Body params:

    ```json
    {
        "grant_type": "refresh_token",
        "refresh_token": "the-refresh-token",
        "client_id": "2",
        "client_secret": "WyDZKxCLg59y6p5W50O2MOqQQLhNu6BhoOySVGu4",
        "scope": "*"
    }
    ```

    Refresh tokes expire in **_180 days_**. The response with have the same format as the one getting an access token.
    A new access token and refresh token will be sent back.

## 1. Eligible NIDs

### Family Tree

-   Request Number: 1
-   Request Type: `GET`
-   URL: `/api/webservices/high-schools/fetch-children`
-   Body params:
    ```
    - form_taker_nid : uuid | required
    - is_form_taker_in_family_tree : boolean | required | true:false (1:0)
    - student_nid: uuid | required_if:is_form_taker_in_family_tree,0
    ```

::: tip Success (In Family Tree)
Request
:::

```json
{
    "form_taker_nid": "J90807071M",
    "is_form_taker_in_family_tree": true
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "legal_guardian": {
                "form_taker_name": "Oresti",
                "form_taker_surname": "Rajdho",
                "form_taker_nid": "F50326039M",
                "form_taker_gender": "M",
                "form_taker_birthdate": "1955-03-26",
                "form_taker_citizenships": ["ALB"],
                "form_taker_email": "",
                "form_taker_phone_number": "",
                "form_taker_student_relation": ""
            },
            "eligible_students": [
                {
                    "student_name": "Akil",
                    "father_name": "Oresti",
                    "student_surname": "Rajdho",
                    "student_nid": "I70908051K",
                    "student_gender": "M",
                    "student_birthdate": "1987-09-08",
                    "student_birthplace": "Kavajë, Durrës",
                    "student_citizenships": ["ALB"]
                }
            ],
            "cities": [
                {
                    "id": "d9cd5106-797b-4371-9880-b9a8c98d1fdd",
                    "name": "Tirane",
                    "elementary_schools": [
                        {
                            "id": "4ec8b35f-5f34-4bb1-99fe-7abcfbafee89",
                            "name": "Ahmet Gashi (Zyra Vendore Arsimore Tiranë)"
                        },
                        {
                            "id": "6046d8e4-dce9-4175-999f-41857db64fbc",
                            "name": "Shkolla 5 Maji (Zyra Vendore Arsimore Tiranë)"
                        },
                        {
                            "id": "ae66ea11-8021-4919-bcd6-6e5a56d65d62",
                            "name": "Flamuri Vlore (Zyra Vendore Arsimore Vlorë-Himarë)"
                        }
                    ]
                },
                {
                    "id": "f363c416-0df8-45d1-872d-2c4bece66362",
                    "name": "Bashkia Durresit",
                    "elementary_schools": []
                }
            ],
            "minorities": [
                {
                    "id": "0ff853be-dc9d-4120-90fd-0dd6ea985e02",
                    "name": "Minoriteti Grek"
                },
                {
                    "id": "170f7bf4-71ac-4418-973f-1b4c71e95627",
                    "name": "Minoriteti Malazez"
                },
                {
                    "id": "3d830f46-e2b3-47c6-b54d-eaf33bece321",
                    "name": "Minoriteti Arumun (Vllah)"
                },
                {
                    "id": "4865df73-0711-441b-8c01-9af05ad81458",
                    "name": "Minoriteti Rom"
                },
                {
                    "id": "9cf5afc7-0f8e-4680-bd55-c4301f00fe0b",
                    "name": "Minoriteti Maqedonas"
                }
            ],
            "school_types": [
                {
                    "id": "27f85a72-6fe7-4f18-af76-d42e22d1235f",
                    "name": "Artistike",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                },
                {
                    "id": "37baa9e5-8c08-4af9-b553-f1323f86a055",
                    "name": "Sportive",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        },
                        {
                            "id": "c3f66a44-3a5f-4e2a-a485-3625a1e0bde7",
                            "name": "Raport mjekësor",
                            "key": "medical_report",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                },
                {
                    "id": "938352ca-de4e-47c9-a49c-9720f630375a",
                    "name": "Profesionale",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                },
                {
                    "id": "b6b35a05-b82c-4095-9fcb-c371a3358a40",
                    "name": "I/E përgjithshme",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                }
            ],
            "courts": []
        }
    ]
}
```

::: tip Error (Wrong NID In Family Tree)
Request
:::

```json
{
    "form_taker_nid": "I00000000I",
    "is_form_taker_in_family_tree": true
}
```

::: tip Error (Wrong NID In Family Tree)
Response
:::

```json
{
    "message": "Personi me këtë NID nuk ekziston!",
    "status": 404,
    "data": []
}
```

::: tip Error (Missing NID In Family Tree)
Request
:::

```json
{
    "form_taker_nid": "",
    "is_form_taker_in_family_tree": true
}
```

::: tip Error (Missing NID In Family Tree)
Response
:::

```json
{
    "message": "NID i personit që po plotëson formularin është i kërkuar!",
    "status": 2,
    "data": [
        {
            "form_taker_nid": [
                "NID i personit që po plotëson formularin është i kërkuar!"
            ]
        }
    ]
}
```

### Out of Family Tree

::: tip Success
Request
:::

```json
{
    "form_taker_nid": "J90807071M",
    "is_form_taker_in_family_tree": false,
    "student_nid": "I70908051K"
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "legal_guardian": {
                "form_taker_name": "Oresti",
                "form_taker_surname": "Rajdho",
                "form_taker_nid": "F50326039M",
                "form_taker_gender": "M",
                "form_taker_birthdate": "1955-03-26",
                "form_taker_citizenships": ["ALB"],
                "form_taker_email": "",
                "form_taker_phone_number": "",
                "form_taker_student_relation": ""
            },
            "eligible_students": [
                {
                    "student_name": "Akil",
                    "father_name": "Oresti",
                    "student_surname": "Rajdho",
                    "student_nid": "I70908051K",
                    "student_gender": "M",
                    "student_birthdate": "1987-09-08",
                    "student_birthplace": "Kavajë, Durrës",
                    "student_citizenships": ["ALB"]
                }
            ],
            "cities": [
                {
                    "id": "d9cd5106-797b-4371-9880-b9a8c98d1fdd",
                    "name": "Tirane",
                    "elementary_schools": [
                        {
                            "id": "4ec8b35f-5f34-4bb1-99fe-7abcfbafee89",
                            "name": "Ahmet Gashi (Zyra Vendore Arsimore Tiranë)"
                        },
                        {
                            "id": "6046d8e4-dce9-4175-999f-41857db64fbc",
                            "name": "Shkolla 5 Maji (Zyra Vendore Arsimore Tiranë)"
                        },
                        {
                            "id": "ae66ea11-8021-4919-bcd6-6e5a56d65d62",
                            "name": "Flamuri Vlore (Zyra Vendore Arsimore Vlorë-Himarë)"
                        }
                    ]
                },
                {
                    "id": "f363c416-0df8-45d1-872d-2c4bece66362",
                    "name": "Bashkia Durresit",
                    "elementary_schools": []
                }
            ],
            "minorities": [
                {
                    "id": "0ff853be-dc9d-4120-90fd-0dd6ea985e02",
                    "name": "Minoriteti Grek"
                },
                {
                    "id": "170f7bf4-71ac-4418-973f-1b4c71e95627",
                    "name": "Minoriteti Malazez"
                },
                {
                    "id": "21b697d1-5619-40db-910d-151cb82c2538",
                    "name": null
                },
                {
                    "id": "3d830f46-e2b3-47c6-b54d-eaf33bece321",
                    "name": "Minoriteti Arumun (Vllah)"
                },
                {
                    "id": "4865df73-0711-441b-8c01-9af05ad81458",
                    "name": "Minoriteti Rom"
                },
                {
                    "id": "9cf5afc7-0f8e-4680-bd55-c4301f00fe0b",
                    "name": "Minoriteti Maqedonas"
                }
            ],
            "school_types": [
                {
                    "id": "27f85a72-6fe7-4f18-af76-d42e22d1235f",
                    "name": "Artistike",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                },
                {
                    "id": "37baa9e5-8c08-4af9-b553-f1323f86a055",
                    "name": "Sportive",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        },
                        {
                            "id": "c3f66a44-3a5f-4e2a-a485-3625a1e0bde7",
                            "name": "Raport mjekësor",
                            "key": "medical_report",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                },
                {
                    "id": "938352ca-de4e-47c9-a49c-9720f630375a",
                    "name": "Profesionale",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                },
                {
                    "id": "b6b35a05-b82c-4095-9fcb-c371a3358a40",
                    "name": "I/E përgjithshme",
                    "documents": [
                        {
                            "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                            "name": "Dëftesa e lirimit",
                            "key": "elementary_school_leaving_certificate",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                }
            ],
            "courts": [
                {
                    "id": "edcdcdb6-5e07-47f0-8cbd-70d90727fe8a",
                    "name": "Gjykata Tiranë"
                },
                {
                    "id": "c82f9a32-ef00-4cd2-bdf0-1258e7b51ee2",
                    "name": "Gjykata Durrës"
                }
            ]
        }
    ]
}
```

::: tip Error (Wrong NID Out of Family Tree)
Request
:::

```json
{
    "form_taker_nid": "I00000000I",
    "is_form_taker_in_family_tree": true,
    "student_nid": "I00000001I"
}
```

::: tip Error (Wrong NID Out of Family Tree)
Response
:::

```json
{
    "message": "Personi me këtë NID nuk ekziston!",
    "status": 404,
    "data": []
}
```

::: tip Error (Missing Form Taker NID Out of Family Tree)
Request
:::

```json
{
    "form_taker_nid": "",
    "is_form_taker_in_family_tree": true,
    "student_nid": "I70908051K"
}
```

::: tip Error (Missing NID Out of Family Tree)
Response
:::

```json
{
    "message": "NID i personit që po plotëson formularin është i kërkuar!",
    "status": 2,
    "data": [
        {
            "form_taker_nid": [
                "NID i personit që po plotëson formularin është i kërkuar!"
            ]
        }
    ]
}
```

::: tip Error (Missing Student NID Out of Family Tree)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": ""
}
```

::: tip Error (Missing NID Out of Family Tree)
Response
:::

```json
{
    "message": "NID i nxënësit është i kërkuar!\n",
    "status": 2,
    "data": [
        {
            "student_nid": ["NID i nxënësit është i kërkuar!"]
        }
    ]
}
```

::: tip Error (Student not found in Civil State)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": "F50232260394M"
}
```

::: tip Error (Student not found in Civil State)
Response
:::

```json
{
    "message": "Nxënësi nuk u gjend në Gjendjen Civile!",
    "status": 404,
    "data": []
}
```

::: tip Error (Student invalid birthday range)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": "F502322603M"
}
```

::: tip Error (Student invalid birthday range)
Response
:::

```json
{
    "message": "Ky NID nuk përmbush kriteret për tu regjistruar në klasën e parë!",
    "status": 422,
    "data": []
}
```

::: tip Error (Student already registered)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": "F502322603M"
}
```

::: tip Error (Student already registered)
Response
:::

```json
{
    "message": "Ky NID është i regjistruar!",
    "status": 409,
    "data": []
}
```

## 2. Fetch schools

-   Request Number: 2
-   Request Type: `GET`
-   URL: `/api/webservices/high-schools`
-   Body params:

    ```
    - elementary_school_id : uuid | nullable
    - school_type_id : uuid | required
    ```

::: tip Success
Request
:::

```json
{
    "elementary_school_id": "35830b15-c45f-4062-a8ba-5d5ca0735c93",
    "school_type_id": "27f85a72-6fe7-4f18-af76-d42e22d1235f"
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "allowed_schools": [
                {
                    "id": "c509da7b-2373-4c57-91f1-8a0dcb39a4a8",
                    "name": "Shkolla Sami Frasheri (Zyra Vendore Arsimore Tiranë)"
                }
            ],
            "private_high_schools": []
        }
    ]
}
```

## 3. Fetch school elective subjects

-   Request Number: 3
-   Request Type: `GET`
-   URL: `/api/webservices/subjects`
-   Body params:

    ```
    - school_id : uuid | required
    ```

::: tip Success
Request
:::

```json
{
    "school_id": "35830b15-c45f-4062-a8ba-5d5ca0735c93"
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "id": "0900b3d5-d7f3-47c0-9cb1-8595830fb11b",
            "subject_name": "Programim në php",
            "fusha_e_te_nxenit": "Teknologji dhe TIK",
            "is_foreign_language": false
        },
        {
            "id": "10c7186d-f812-463c-9500-38269b7bd1a4",
            "subject_name": "Shërbime pastrimi 2",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "194c7fa4-6245-4f93-ae2c-0c7f91196ba5",
            "subject_name": "Gjeografi",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "1f4c06cd-486b-4b0c-bc91-4f2d2097ff74",
            "subject_name": "Dituri Natyre",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "1fd0fcdb-303d-49f0-8220-72c3ca8c0651",
            "subject_name": "Sherbime Komunitare",
            "fusha_e_te_nxenit": "Kultura profesionale",
            "is_foreign_language": false
        },
        {
            "id": "26355fdc-b090-4d32-ad87-83a16c0c6480",
            "subject_name": "Organizimi i strukturave hoteliere dhe gastronomike",
            "fusha_e_te_nxenit": "Lēndēt profesionale",
            "is_foreign_language": false
        },
        {
            "id": "271c9651-b62e-45b7-b96f-41c1566afdbb",
            "subject_name": "Muzikë",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "27654ddb-675c-4551-acc6-12735ac2e646",
            "subject_name": "Shkenca Sociale",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "29cbc0f2-ef18-4386-a60a-86781a27deeb",
            "subject_name": "Biologji",
            "fusha_e_te_nxenit": "Shkencat e natyrës",
            "is_foreign_language": false
        },
        {
            "id": "2ebee080-7212-4b10-83a2-3014c429dc69",
            "subject_name": "Pune Vullnetare",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "33363ad3-ef41-463e-b4ff-084eb4bb878b",
            "subject_name": "Shërbime në recepsion 1",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "33e4baa2-ef69-489a-9930-36608b99b4ee",
            "subject_name": "Kuzhinë dhe pastiçeri",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "37945322-16cd-4c22-a1c2-c018c7674a16",
            "subject_name": "Italisht",
            "fusha_e_te_nxenit": "Gjuhët dhe komunikimi",
            "is_foreign_language": true
        },
        {
            "id": "3df9bf27-c06a-447d-9819-f91ca8c118f3",
            "subject_name": "Gjuhe Shqipe",
            "fusha_e_te_nxenit": "Gjuhët dhe komunikimi",
            "is_foreign_language": false
        },
        {
            "id": "419ddfab-47cd-4237-bb71-eda70efcda8b",
            "subject_name": "Edukim Fizik",
            "fusha_e_te_nxenit": "Edukim fizik, sporti dhe shëndeti",
            "is_foreign_language": false
        },
        {
            "id": "45f39553-1345-4d2e-aae1-69424befd5d4",
            "subject_name": "Matematike",
            "fusha_e_te_nxenit": "Matematikë",
            "is_foreign_language": false
        },
        {
            "id": "49d721c7-ecc7-4fe1-931d-b596c7a47250",
            "subject_name": "Kuzhinë",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "4c5c6b66-661d-4212-8174-bc976edb29e1",
            "subject_name": "TIK",
            "fusha_e_te_nxenit": "Teknologji dhe TIK",
            "is_foreign_language": false
        },
        {
            "id": "4c71d746-449f-490d-a63d-cd109fad5dbd",
            "subject_name": "Përgatitja e ushqimeve fast -food",
            "fusha_e_te_nxenit": "Modulet e praktikēs profesionale me zgjedhje tē detyruar",
            "is_foreign_language": false
        },
        {
            "id": "4e756428-390e-4b14-a000-55f95586bd41",
            "subject_name": "Fiskulture",
            "fusha_e_te_nxenit": "Edukim fizik, sporti dhe shëndeti",
            "is_foreign_language": false
        },
        {
            "id": "54c0d5e3-2fdf-4baf-97a9-3f750fa178ca",
            "subject_name": "Përgatitja e sallës së restorantit dhe shërbimi për festa",
            "fusha_e_te_nxenit": "Modulet e praktikēs profesionale me zgjedhje tē detyruar",
            "is_foreign_language": false
        },
        {
            "id": "55fe7c39-b5b0-40d1-a46f-a32f839cb94f",
            "subject_name": "Shërbime në restorant",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "5775f06c-0d3f-43cb-878b-d3d288b3f4ab",
            "subject_name": "Shërbimi në pushimet e kafesë",
            "fusha_e_te_nxenit": "Modulet e praktikēs profesionale me zgjedhje tē detyruar",
            "is_foreign_language": false
        },
        {
            "id": "5d288a61-9be7-4e6c-9cff-a54c1bdb376d",
            "subject_name": "Qytetari",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "5e7ca477-0d86-4ba0-ba5f-4d6d78cb1ed2",
            "subject_name": "Përgatitja e një menyje me asortimente të zonës me bazë mishi",
            "fusha_e_te_nxenit": "Modulet e praktikēs profesionale me zgjedhje tē detyruar",
            "is_foreign_language": false
        },
        {
            "id": "724d31fa-4014-40e5-9b1f-e270ed2a770f",
            "subject_name": "Histori",
            "fusha_e_te_nxenit": "Shoqëria dhe mjedisi",
            "is_foreign_language": false
        },
        {
            "id": "828f694e-9788-46d4-a7b6-6082861b41de",
            "subject_name": "Teknologji",
            "fusha_e_te_nxenit": "Teknologji dhe TIK",
            "is_foreign_language": false
        },
        {
            "id": "8a3a9192-f83f-40fe-a3fb-32f2ddac0b68",
            "subject_name": "Komunikimi në gjuhë të huaj në hotel",
            "fusha_e_te_nxenit": "Modulet e praktikēs profesionale me zgjedhje tē detyruar",
            "is_foreign_language": false
        },
        {
            "id": "8fd0d233-1487-4768-a985-45f8d501fd22",
            "subject_name": "Fizikë",
            "fusha_e_te_nxenit": "Shkencat e natyrës",
            "is_foreign_language": false
        },
        {
            "id": "91e9317e-54da-4ed2-bc67-fcbfc12fa5d0",
            "subject_name": "Pune Dore",
            "fusha_e_te_nxenit": "Arte",
            "is_foreign_language": false
        },
        {
            "id": "9d0c478d-bf4c-4ff2-bfca-a4f9520204e0",
            "subject_name": "Anglisht",
            "fusha_e_te_nxenit": "Gjuhët dhe komunikimi",
            "is_foreign_language": true
        },
        {
            "id": "a24aea9b-fc10-4ca5-a70d-bfb18e226957",
            "subject_name": "Shërbime pastrimi 1",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "a6575ac4-72a9-45c8-9478-3386666e8788",
            "subject_name": "Shërbime në restorant dhe bar",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "c37fe289-a8d5-48c8-adb2-f1c610ac5aef",
            "subject_name": "Kercim",
            "fusha_e_te_nxenit": "Arte",
            "is_foreign_language": false
        },
        {
            "id": "d00306f5-3984-4b9e-8aae-904f78d2a46a",
            "subject_name": "Turizmi dhe mjedisi",
            "fusha_e_te_nxenit": "Lēndēt profesionale",
            "is_foreign_language": false
        },
        {
            "id": "d71527a9-8756-40ef-8ba5-dd9204c5883a",
            "subject_name": "Ushqim dhe pije",
            "fusha_e_te_nxenit": "Lēndēt profesionale",
            "is_foreign_language": false
        },
        {
            "id": "d7b5845c-ece8-4377-96c2-ad28157b266d",
            "subject_name": "Gjermanisht",
            "fusha_e_te_nxenit": "Gjuhët dhe komunikimi",
            "is_foreign_language": true
        },
        {
            "id": "db5284c6-f731-413f-a0cf-ec283e4c0dd6",
            "subject_name": "Trashëgimi kulturore dhe historike",
            "fusha_e_te_nxenit": "Lēndēt profesionale",
            "is_foreign_language": false
        },
        {
            "id": "df16dddb-9588-4a4c-a3b2-a5ad6e7ed39a",
            "subject_name": "Art Pamor",
            "fusha_e_te_nxenit": "Arte",
            "is_foreign_language": false
        },
        {
            "id": "e6ca9636-7c3b-4ca4-868a-a27e196e0319",
            "subject_name": "Përgatitja e një menyje me asortimente të zonës me bazë peshku",
            "fusha_e_te_nxenit": "Modulet e praktikēs profesionale me zgjedhje tē detyruar",
            "is_foreign_language": false
        },
        {
            "id": "ed8b8cf0-4fe1-4ddf-b723-5e61c1711042",
            "subject_name": "Shërbime në recepsion 2",
            "fusha_e_te_nxenit": "Module tē detyruar tē praktikēs profesionale",
            "is_foreign_language": false
        },
        {
            "id": "f46889f0-d937-4580-91fc-26719ab02730",
            "subject_name": "Kimi",
            "fusha_e_te_nxenit": "Shkencat e natyrës",
            "is_foreign_language": false
        },
        {
            "id": "f958d8fb-85b0-4e03-9421-80bb7b25579b",
            "subject_name": "Letërsi",
            "fusha_e_te_nxenit": "Gjuhët dhe komunikimi",
            "is_foreign_language": false
        }
    ]
}
```

## 4. Store Student Record

-   Request Number: 4
-   Request Type: `POST`
-   URL: `/api/webservices/high-schools/register-student`

::: tip Success
Request
:::

```json
{
    "form_taker_name": "Oresti",
    "form_taker_surname": "Rajdho",
    "form_taker_nid": "F50326039M",
    "form_taker_gender": "M",
    "form_taker_birthdate": "1955-03-26",
    "form_taker_citizenships": ["ALB"],
    "form_taker_email": "mail@mail2.com",
    "form_taker_phone_number": "0689585985",
    "is_form_taker_in_family_tree": true,
    "form_taker_student_relation": "Prind",
    "student_name": "Akil",
    "father_name": "Oresti",
    "student_surname": "Rajdho",
    "student_nid": "I70908051K",
    "student_gender": "M",
    "student_birthdate": "1987-09-08",
    "student_birthplace": "Kavajë, Durrës",
    "student_citizenships": ["ALB"],
    "elementary_school_id": "4ec8b35f-5f34-4bb1-99fe-7abcfbafee89",
    "school_id": "c509da7b-2373-4c57-91f1-8a0dcb39a4a8",
    "school_type_id": "27f85a72-6fe7-4f18-af76-d42e22d1235f",
    "first_elective_subject_id": "29cbc0f2-ef18-4386-a60a-86781a27deeb",
    "second_elective_subject_id": "3df9bf27-c06a-447d-9819-f91ca8c118f3",
    "minority_id": "0ff853be-dc9d-4120-90fd-0dd6ea985e02"
    "foreign_elementary_school": false,
    "is_public": true
}
```

-   In addition to these fields, you will be able to receive the document types based on the school type,
    and should pass the key as the parameter name for the file uploads:
    ```json
    {
        "school_types": [
            {
                "id": "b6b35a05-b82c-4095-9fcb-c371a3358a40",
                "name": "I/E përgjithshme",
                "documents": [
                    {
                        "id": "b3567fbb-d881-453b-a60a-1fd2587f60d0",
                        "name": "Dëftesa e lirimit",
                        "key": "elementary_school_leaving_certificate",
                        "format": "PDF",
                        "max_size": "5"
                    }
                ]
            }
        ]
    }
    ```
    Params:
    ```json
    {
        "elementary_school_leaving_certificate": "File"
    }
    ```

::: tip Success
Request
:::

```json
{
    "form_taker_name": "Oresti",
    "form_taker_surname": "Rajdho",
    "form_taker_nid": "F50326039M",
    "form_taker_gender": "M",
    "form_taker_birthdate": "1955-03-26",
    "form_taker_citizenships": ["ALB"],
    "form_taker_email": "mail@mail2.com",
    "form_taker_phone_number": "0689585985",
    "is_form_taker_in_family_tree": false,
    "form_taker_student_relation": "Prind",
    "student_name": "Akil",
    "father_name": "Oresti",
    "student_surname": "Rajdho",
    "student_nid": "I70908051K",
    "student_gender": "M",
    "student_birthdate": "1987-09-08",
    "student_birthplace": "Kavajë, Durrës",
    "student_citizenships": ["ALB"],
    "elementary_school_id": "4ec8b35f-5f34-4bb1-99fe-7abcfbafee89",
    "school_id": "c509da7b-2373-4c57-91f1-8a0dcb39a4a8",
    "first_elective_subject_id": "29cbc0f2-ef18-4386-a60a-86781a27deeb",
    "second_elective_subject_id": "3df9bf27-c06a-447d-9819-f91ca8c118f3",
    "minority_id": "0ff853be-dc9d-4120-90fd-0dd6ea985e02",
    "guardianship_document": "File",
    "elementary_school_leaving_certificate": "File",
    "foreign_elementary_school": false,
    "is_public": true
}
```

::: Tip Success
Response
:::

```json
{
    "message": "Nxënësi u regjistrua me sukses!",
    "status": 200,
    "data": [
        {
            "tracking_number": "eff3f693-dd73-4215-be52-27698fbcc93c"
        }
    ]
}
```

::: Tip Error
Response
:::

```json
{
    "message": "{\"form_taker_name\":[\"Fusha është e kërkuar!\"],\"form_taker_surname\":[\"Fusha është e kërkuar!\"],\"form_taker_nid\":[\"Fusha është e kërkuar!\"],\"form_taker_gender\":[\"Fusha është e kërkuar!\"],\"form_taker_birthdate\":[\"Fusha është e kërkuar!\"],\"form_taker_citizenships\":[\"Fusha është e kërkuar!\"],\"form_taker_email\":[\"The form taker email field is required when form taker phone number is not present.\"],\"form_taker_phone_number\":[\"The form taker phone number field is required when form taker email is not present.\"],\"is_form_taker_in_family_tree\":[\"Fusha është e kërkuar!\"],\"student_name\":[\"Fusha është e kërkuar!\"],\"father_name\":[\"Fusha është e kërkuar!\"],\"student_surname\":[\"Fusha është e kërkuar!\"],\"student_nid\":[\"Fusha është e kërkuar!\"],\"student_gender\":[\"Fusha është e kërkuar!\"],\"student_birthdate\":[\"Fusha është e kërkuar!\"],\"student_citizenships\":[\"Fusha është e kërkuar!\"],\"elementary_school_id\":[\"Fusha është e kërkuar!\"],\"school_id\":[\"Fusha është e kërkuar!\"],\"first_elective_subject_id\":[\"Fusha është e kërkuar!\"],\"second_elective_subject_id\":[\"Fusha është e kërkuar!\"]}\n",
    "status": 2,
    "data": [
        {
            "form_taker_name": ["Fusha është e kërkuar!"],
            "form_taker_surname": ["Fusha është e kërkuar!"],
            "form_taker_nid": ["Fusha është e kërkuar!"],
            "form_taker_gender": ["Fusha është e kërkuar!"],
            "form_taker_birthdate": ["Fusha është e kërkuar!"],
            "form_taker_citizenships": ["Fusha është e kërkuar!"],
            "form_taker_email": [
                "The form taker email field is required when form taker phone number is not present."
            ],
            "form_taker_phone_number": [
                "The form taker phone number field is required when form taker email is not present."
            ],
            "is_form_taker_in_family_tree": ["Fusha është e kërkuar!"],
            "student_name": ["Fusha është e kërkuar!"],
            "father_name": ["Fusha është e kërkuar!"],
            "student_surname": ["Fusha është e kërkuar!"],
            "student_nid": ["Fusha është e kërkuar!"],
            "student_gender": ["Fusha është e kërkuar!"],
            "student_birthdate": ["Fusha është e kërkuar!"],
            "student_citizenships": ["Fusha është e kërkuar!"],
            "elementary_school_id": ["Fusha është e kërkuar!"],
            "school_id": ["Fusha është e kërkuar!"],
            "first_elective_subject_id": ["Fusha është e kërkuar!"],
            "second_elective_subject_id": ["Fusha është e kërkuar!"],
            "elementary_school_leaving_certificate": [
                "Dokumenti është i kërkuar!"
            ]
        }
    ]
}
```

## 4. Track Student Application

-   Request Number: 4
-   Request Type: `GET`
-   URL: `/api/webservices/track/${TRACKING_NUMBER}`

::: Tip Success
Request
:::

URL : `/api/webservices/track/485d3bec-6b08-4358-bc52-cedcbccd89e3`'

::: Tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "full_name": "Akil Rajdho",
            "school_name": "Shkolla Sami Frasheri (Zyra Vendore Arsimore Tiranë)",
            "status": "Në pritje dokumentash"
        }
    ]
}
```

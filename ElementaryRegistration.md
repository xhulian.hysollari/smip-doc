# API

## Password Authorization

E-Albania user credentials:

/*REMOVED*/

### Getting Access Token

-   Request Type: `POST`
-   URL: `/oauth/token`
-   Headers:

    ```
    Content-Type: application/json
    ```

-   Body params:

    ```json
    {
        "grant_type": "password",
        "client_id": "2",
        "client_secret": "WyDZKxCLg59y6p5W50O2MOqQQLhNu6BhoOySVGu4",
        "username": "e-albania@api.al",
        "password": "ApiEAlbania@2019",
        "scope": "*"
    }
    ```

::: tip Success Reponse :::

```json
{
    "token_type": "Bearer",
    "expires_in": 7775999,
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI2NzJhOWU5OTRiMzYxY2JhMmM1N2FkOTJiYWYxOGM2ZDdiZmYxNGE4Y2VjZjg4ZTJhMGMwOTY3MWM4ZDc0MTkwNjEzMmQ0Y2RkMDU3NTVkIn0.eyJhdWQiOiIyIiwianRpIjoiMjY3MmE5ZTk5NGIzNjFjYmEyYzU3YWQ5MmJhZjE4YzZkN2JmZjE0YThjZWNmODhlMmEwYzA5NjcxYzhkNzQxOTA2MTMyZDRjZGQwNTc1NWQiLCJpYXQiOjE1NjMzNTA5NzMsIm5iZiI6MTU2MzM1MDk3MywiZXhwIjoxNTcxMTI2OTcyLCJzdWIiOiIyM2EwNGRlZC1kNmY0LTRjMDctOTM1Yy1lNzBkNzdiYjk3OGMiLCJzY29wZXMiOlsiKiJdfQ.qcw4ejDNJfBvIT3CzlZqfr38VmKMTyXT0nD2siDlWmlX2CPZ9xJmUxjDtZQgRGZkV09KnBxhLg_GHYqkHlbCzeT6yi0RJrAYXdxiEJgXsfV0nB8LL7W8sExklrw2dK4N3WBx_Kbu3vGgebdneJFaVAm6SGNE7IFSucPiUjDV9jIL3OmUgf4tdUWIF3uxcmQuHH9Zz9jHIVml1EVNtjqcub1GgKdgGTB5ZKFo2lHKj3Z5F8tBwT72i9tp9S1ewjnNxTZC5xuIs7PwXUUrFg_OyYXehH2N-O6wmRdPAic8Z4CGqOZY9aWp-Ikv-RjBIxLlauTlQkJf3BxfCvw67TYaO8-R7NPmgOVde558q7NiEhNSgS8LPEIwEaiApR0F81BO8OA0PBD8wgNQVU0PZw-JGJBDvItwWvKvCG1QSy6JYGHaCrqkgyRSkgyPcUR5CinxdYbzh8MnyujDcq7Un-IM-x6fQqbDN6Yf5QX7Gbqvspfjb-UPAzgF323-33aF6vFHGlPsiKsc1QE3Dxyap480Abs5sfvYzY-xEuq3wwo97DkHyScWuHzK_GM3R-aiMuMYZE5qDFVFxKARGoH5e15VG-mzzjeKKROZGeNERtMm8eAaxEtOZvDFWKFLu6kKDKa8Byf7eQWAZi8Gr1NYY36FlTwfV6SGji3KCLqY3BBk9a8",
    "refresh_token": "def50200a4db012cbd30347d977b5f45e6ce9dce544893709a164af396cd4fe9c955a06c2af1ccbe63d8e5e971ede1646a12b284ec9e7e930475bc5f2006e2705978acdaec82608fe224ab3fca5a984507fde5e0017b36857ee1d3655f2ffae30699cb15e7a4b702e1374b6c9908f9529ee6eadba0fef8b0b8e808cf18e886408c5a91713f54e0113e9cb4c596df325234c16000b8801330dae65e4ad081bda18be0bb708f86d248e073d6fa6189fa9b7b3b2ca259813647e93042094890d4e2ae53da19b584cb63eeecbeea86fe648ed32b369808cc8cb34f037ea17952e7653d66c56644541842ef9d812dad5d6d67967776a02549e1b2de4e96d0f5fdef86e5f619b9435c6a81650b79d27e3cfb62cefce4282858ef6ed14a1de28c2e34b31d12e983e239926b615fa4562be84aff27c80d7dcedba8fa8b13841f5467bae4670caa16506987b2d1374bf2801c3118a2d2cd876a38c6fa8287cd9b4cd56fff8af906f7ce3fadaa7e4c1f77c9e8fedab4674220a319b0e629e757ec1eb7af1022a4577807f2795ae9"
}
```

Each consecutive request should the include the `Authorization` header, using the bearer scheme:

```
   Authorization: Bearer the-access-token
```

Access tokens are valid for **_90 days_**. Once the token has expired, it can be refreshed by using the refresh token:

### Refreshing tokens

-   Request Type: `POST`
-   URL: `/oauth/token`
-   Headers:

    ```
    Content-Type: application/json
    ```

-   Body params:

    ```json
    {
        "grant_type": "refresh_token",
        "refresh_token": "the-refresh-token",
        "client_id": "2",
        "client_secret": "WyDZKxCLg59y6p5W50O2MOqQQLhNu6BhoOySVGu4",
        "scope": "*"
    }
    ```

    Refresh tokes expire in **_180 days_**. The response with have the same format as the one getting an access token.
    A new access token and refresh token will be sent back.

## 1. Eligible NIDs

### Family Tree

-   Request Number: 1
-   Request Type: `GET`
-   URL: `/api/webservices/elementary/fetch-children`
-   Body params:
    ```
    - form_taker_nid : uuid | required
    - is_form_taker_in_family_tree : boolean | required | true:false (1:0)
    - student_nid: uuid | required_if:is_form_taker_in_family_tree,0
    ```

::: tip Success (In Family Tree)
Request
:::

```json
{
    "form_taker_nid": "J90807071M",
    "is_form_taker_in_family_tree": true
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "legal_guardian": {
                "form_taker_name": "Oresti",
                "form_taker_surname": "Rajdho",
                "form_taker_nid": "J90807071M",
                "form_taker_gender": "M",
                "form_taker_birthdate": "1955-03-26",
                "form_taker_citizenships": ["Shqipetare"],
                "form_taker_email": "email-qe-meret-nga-ealbania",
                "form_taker_phone_number": "Nr Tel Qe Meret nga Ealbania",
                "form_taker_student_relation": ""
            },
            "eligible_students": [
                {
                    "student_nid": "I70908051K",
                    "activeApplication": true,
                    "message": "Ju keni nje aplikim aktiv ne shkollen 9-Vjecare Serice (Zyra Vendore Arsimore Elbasan) me numer gjurmimi 123ac4de-6338-498d-b630-01708ceb0fdd"
            },
                },
                {
                    "student_name": "Kristian",
                    "father_name": "Oresti",
                    "student_surname": "Rajdho",
                    "student_nid": "I70908051K",
                    "student_gender": "M",
                    "student_birthdate": "1987-09-08",
                    "student_birthplace": "Kavajë",
                    "student_citizenships": ["Shqipetare"]
                }
            ],
            "cities": [
                {
                    "id": "4c1b1963-b4bd-4a19-8990-e20ffb1e32c6",
                    "name": "Tirane",
                    "administrative_units": [
                        {
                            "id": "0c3e40bb-3c4d-4354-8730-e26da281df3e",
                            "name": "Njesia Administrative 11"
                        },
                        {
                            "id": "4ce20f61-35ea-4066-931f-d690e55df77c",
                            "name": "Njesia Administrative 7"
                        },
                        {
                            "id": "a5054803-4c61-410b-bec0-7c85eb7b5956",
                            "name": "Njesia Administrative 1"
                        }
                    ],
                    "medical_centers": [
                        {
                            "id": "2340b219-8657-48be-b3c9-1da1e222d391",
                            "name": "Qendra Shendetesore ABC"
                        },
                        {
                            "id": "3bf86ef5-66da-4bbb-a487-7a070824618d",
                            "name": "Poliklinika Nr.9"
                        },
                        {
                            "id": "b42656b3-2571-41a4-97a6-78cd1e5bdcf6",
                            "name": "Qendra Shendetesore Bregu i Lumit"
                        }
                    ],
                    "civil_state_offices": [
                        {
                            "id": "faab4f1d-ebbc-4ef3-b460-74ba5ec27117",
                            "name": "Drejtoria e Përgjithshme e Gjendjes Civile"
                        }
                    ],
                    "kindergartens": [
                        {
                            "id": "cab79202-0829-4551-8af2-038a1c2b9dfb",
                            "name": "Kopshti Nr.22"
                        },
                        {
                            "id": "e8fab36a-3dc3-478e-b31a-10797d27ede8",
                            "name": "Kopshti Nr.25/1"
                        }
                    ]
                }
            ],
            "minorities": [
                {
                    "id": "cab79202-0829-4551-8af2-038a1c2b94n",
                    "name": "Grek"
                },
                {
                    "id": "cab79202-0829-4551-8af2-038a1c2b94n",
                    "name": "Rom"
                },
                {
                    "id": "cab79202-0829-4551-8af2-038a1c2b94n",
                    "name": "Egjiptian"
                }
            ],
            "school_types": [
                {
                    "id": "879e3645-6b39-4112-9e2a-864e1d0a1383",
                    "name": "Fillore",
                    "documents": [
                        {
                            "id": "5c74e187-99dc-4316-a8cb-e143e780b4e2",
                            "name": "Vërtetim kopshti",
                            "key": "certificate_of_kindergarten",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                }
            ],
            "courts": [
                {
                    "id": "edcdcdb6-5e07-47f0-8cbd-70d90727fe8a",
                    "name": "Gjykata Tiranë"
                },
                {
                    "id": "c82f9a32-ef00-4cd2-bdf0-1258e7b51ee2",
                    "name": "Gjykata Durrës"
                }
            ],
            "kindergarten_types": [
                {
                    "key": 0,
                    "value": "Nuk ka bërë kopësht"
                },
                {
                    "key": 1,
                    "value": "Kopësht publik"
                },
                {
                    "key": 2,
                    "value": "Kopësht privat"
                }
            ]
        }
    ]
}
```

::: tip Error (Wrong NID In Family Tree)
Request
:::

```json
{
    "form_taker_nid": "I00000000I",
    "is_form_taker_in_family_tree": true
}
```

::: tip Error (Wrong NID In Family Tree)
Response
:::

```json
{
    "message": "Personi me këtë NID nuk ekziston!",
    "status": 404,
    "data": []
}
```

::: tip Error (Missing NID In Family Tree)
Request
:::

```json
{
    "form_taker_nid": "",
    "is_form_taker_in_family_tree": true
}
```

::: tip Error (Missing NID In Family Tree)
Response
:::

```json
{
    "message": "NID i personit që po plotëson formularin është i kërkuar!",
    "status": 2,
    "data": [
        {
            "form_taker_nid": [
                "NID i personit që po plotëson formularin është i kërkuar!"
            ]
        }
    ]
}
```

### Out of Family Tree

::: tip Success
Request
:::

```json
{
    "form_taker_nid": "J90807071M",
    "is_form_taker_in_family_tree": false,
    "student_nid": "I70908051K"
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "legal_guardian": {
                "form_taker_name": "Oresti",
                "form_taker_surname": "Rajdho",
                "form_taker_nid": "J90807071M",
                "form_taker_gender": "M",
                "form_taker_birthdate": "1955-03-26",
                "form_taker_citizenships": ["Shqipetare"],
                "form_taker_email": "email-qe-meret-nga-ealbania",
                "form_taker_phone_number": "Nr Tel Qe Meret nga Ealbania",
                "form_taker_student_relation": ""
            },
            "eligible_students": [
                {
                    "student_name": "Akil",
                    "father_name": "Oresti",
                    "student_surname": "Rajdho",
                    "student_nid": "I70908051K",
                    "student_gender": "M",
                    "student_birthdate": "1987-09-08",
                    "student_birthplace": "Kavajë",
                    "student_citizenships": ["Shqipetare"]
                }
            ],
            "cities": [
                {
                    "id": "4c1b1963-b4bd-4a19-8990-e20ffb1e32c6",
                    "name": "Tirane",
                    "administrative_units": [
                        {
                            "id": "0c3e40bb-3c4d-4354-8730-e26da281df3e",
                            "name": "Njesia Administrative 11"
                        },
                        {
                            "id": "4ce20f61-35ea-4066-931f-d690e55df77c",
                            "name": "Njesia Administrative 7"
                        },
                        {
                            "id": "a5054803-4c61-410b-bec0-7c85eb7b5956",
                            "name": "Njesia Administrative 1"
                        }
                    ],
                    "medical_centers": [
                        {
                            "id": "2340b219-8657-48be-b3c9-1da1e222d391",
                            "name": "Qendra Shendetesore ABC"
                        },
                        {
                            "id": "3bf86ef5-66da-4bbb-a487-7a070824618d",
                            "name": "Poliklinika Nr.9"
                        },
                        {
                            "id": "b42656b3-2571-41a4-97a6-78cd1e5bdcf6",
                            "name": "Qendra Shendetesore Bregu i Lumit"
                        }
                    ],
                    "civil_state_offices": [
                        {
                            "id": "faab4f1d-ebbc-4ef3-b460-74ba5ec27117",
                            "name": "Drejtoria e Përgjithshme e Gjendjes Civile"
                        }
                    ],
                    "kindergartens": [
                        {
                            "id": "cab79202-0829-4551-8af2-038a1c2b9dfb",
                            "name": "Kopshti Nr.22"
                        },
                        {
                            "id": "e8fab36a-3dc3-478e-b31a-10797d27ede8",
                            "name": "Kopshti Nr.25/1"
                        }
                    ]
                }
            ],
            "minorities": [
                {
                    "id": "cab79202-0829-4551-8af2-038a1c2b94n",
                    "name": "Grek"
                },
                {
                    "id": "cab79202-0829-4551-8af2-038a1c2b94n",
                    "name": "Rom"
                },
                {
                    "id": "cab79202-0829-4551-8af2-038a1c2b94n",
                    "name": "Egjiptian"
                }
            ],
            "school_types": [
                {
                    "id": "879e3645-6b39-4112-9e2a-864e1d0a1383",
                    "name": "Fillore",
                    "documents": [
                        {
                            "id": "5c74e187-99dc-4316-a8cb-e143e780b4e2",
                            "name": "Vërtetim kopshti",
                            "key": "certificate_of_kindergarten",
                            "format": "PDF",
                            "max_size": "5"
                        }
                    ]
                }
            ],
            "courts": [
                {
                    "id": "edcdcdb6-5e07-47f0-8cbd-70d90727fe8a",
                    "name": "Gjykata Tiranë"
                },
                {
                    "id": "c82f9a32-ef00-4cd2-bdf0-1258e7b51ee2",
                    "name": "Gjykata Durrës"
                }
            ],
            "kindergarten_types": [
                {
                    "key": 0,
                    "value": "Nuk ka bërë kopësht"
                },
                {
                    "key": 1,
                    "value": "Kopësht publik"
                },
                {
                    "key": 2,
                    "value": "Kopësht privat"
                }
            ]
        }
    ]
}
```

::: tip Error (Wrong NID Out of Family Tree)
Request
:::

```json
{
    "form_taker_nid": "I00000000I",
    "is_form_taker_in_family_tree": true,
    "student_nid": "I00000001I"
}
```

::: tip Error (Wrong NID Out of Family Tree)
Response
:::

```json
{
    "message": "Personi me këtë NID nuk ekziston!",
    "status": 404,
    "data": []
}
```

::: tip Error (Missing Form Taker NID Out of Family Tree)
Request
:::

```json
{
    "form_taker_nid": "",
    "is_form_taker_in_family_tree": true,
    "student_nid": "I70908051K"
}
```

::: tip Error (Missing NID Out of Family Tree)
Response
:::

```json
{
    "message": "NID i personit që po plotëson formularin është i kërkuar!",
    "status": 2,
    "data": [
        {
            "form_taker_nid": [
                "NID i personit që po plotëson formularin është i kërkuar!"
            ]
        }
    ]
}
```

::: tip Error (Missing Student NID Out of Family Tree)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": ""
}
```

::: tip Error (Missing NID Out of Family Tree)
Response
:::

```json
{
    "message": "NID i nxënësit është i kërkuar!\n",
    "status": 2,
    "data": [
        {
            "student_nid": ["NID i nxënësit është i kërkuar!"]
        }
    ]
}
```

::: tip Error (Student not found in Civil State)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": "F50232260394M"
}
```

::: tip Error (Student not found in Civil State)
Response
:::

```json
{
    "message": "Nxënësi nuk u gjend në Gjendjen Civile!",
    "status": 404,
    "data": []
}
```

::: tip Error (Student invalid birthday range)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": "F502322603M"
}
```

::: tip Error (Student invalid birthday range)
Response
:::

```json
{
    "message": "Ky NID nuk përmbush kriteret për tu regjistruar në klasën e parë!",
    "status": 422,
    "data": []
}
```

::: tip Error (Student already registered)
Request
:::

```json
{
    "form_taker_nid": "F50326039M",
    "is_form_taker_in_family_tree": true,
    "student_nid": "F502322603M"
}
```

::: tip Error (Student already registered)
Response
:::

```json
{
    "message": "Ky NID është i regjistruar!",
    "status": 409,
    "data": []
}
```

## 2. Fetch streets and schools

-   Request Number: 2
-   Request Type: `GET`
-   URL: `/api/webservices/elementary/streets`
-   Body params:

    ```
    - administrative_unit_id : uuid | required
    ```

::: tip Success
Request
:::

```json
{
    "administrative_unit_id": "35830b15-c45f-4062-a8ba-5d5ca0735c93"
}
```

::: tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "streets": [
                {
                    "id": "24c86fd2-7c7e-49c6-be25-c7b8712c4e08",
                    "name": "Rruga Aleksander Voga",
                    "schools": [
                        {
                            "id": "4ec8b35f-5f34-4bb1-99fe-7abcfbafee89",
                            "name": "Ahmet Gashi (Zyra Vendore Arsimore Tiranë)"
                        },
                        {
                            "id": "6046d8e4-dce9-4175-999f-41857db64fbc",
                            "name": "Shkolla 5 Maji (Zyra Vendore Arsimore Tiranë)"
                        }
                    ]
                },
                {
                    "id": "89848e02-de50-493c-a0f1-aeaae8c0a802",
                    "name": "Rruga E Plepave",
                    "schools": [
                        {
                            "id": "6046d8e4-dce9-4175-999f-41857db64fbc",
                            "name": "Shkolla 5 Maji (Zyra Vendore Arsimore Tiranë)"
                        }
                    ]
                }
            ],
            "private_schools": [
                {
                    "id": "ae66ea11-8021-4919-bcd6-6e5a56d65d62",
                    "name": "Flamuri Vlore (Zyra Vendore Arsimore Vlorë-Himarë)"
                }
            ],
            "allowed_schools": []
        }
    ]
}
```

## 3. Store Student Record

-   Request Number: 3
-   Request Type: `POST`
-   URL: `/api/webservices/elementary/register-student`

::: tip Success
Request
:::

```json
{
    "form_taker_name": "Oresti",
    "form_taker_surname": "Rajdho",
    "form_taker_nid": "F50326039M",
    "form_taker_gender": "M",
    "form_taker_birthdate": "1955-03-26",
    "form_taker_citizenships": ["ALB"],
    "form_taker_email": "mail@mail2.com",
    "form_taker_phone_number": "0689585985",
    "is_form_taker_in_family_tree": true,
    "form_taker_student_relation": "Prind",
    "student_name": "Akil",
    "father_name": "Oresti",
    "student_surname": "Rajdho",
    "student_nid": "I70908051K",
    "student_gender": "M",
    "student_birthdate": "1987-09-08",
    "student_birthplace": "Kavajë, Durrës",
    "student_citizenships": ["ALB"],
    "street_id": "89848e02-de50-493c-a0f1-aeaae8c0a802",
    "school_id": "ae66ea11-8021-4919-bcd6-6e5a56d65d62",
    "vaccination_medical_center_id": "a4808b8f-3288-4ef4-9c00-e13874616ebe",
    "civil_state_office_id": "97590ab8-4671-4454-a5d5-85a30d9a7fb8",
    "kindergarten_type": 0,
    "kindergarten_id": "e9fb6acb-ca73-40cb-bae8-0ca986c7e6fb",
    "minority_id": "0ff853be-dc9d-4120-90fd-0dd6ea985e02",
    "court_id": ""
}
```

-   In addition to these fields, you will be able to receive the document types based on the school type,
    and should pass the key as the parameter name for the file uploads:

    ```json
    {
        "school_types": [
            {
                "id": "879e3645-6b39-4112-9e2a-864e1d0a1383",
                "name": "Fillore",
                "documents": [
                    {
                        "id": "2f8af098-70a6-4121-b4e4-993f4ffef2a6",
                        "name": "Dëftesa e klasës së mëparshme",
                        "key": "previous_diploma",
                        "format": "PDF",
                        "max_size": "5"
                    }
                ]
            }
        ]
    }
    ```

    Params:

    ```json
    {
        "previous_diploma": "File"
    }
    ```

::: tip Success
Request
:::

```json
{
    "form_taker_name": "Oresti",
    "form_taker_surname": "Rajdho",
    "form_taker_nid": "F50326039M",
    "form_taker_gender": "M",
    "form_taker_birthdate": "1955-03-26",
    "form_taker_citizenships": ["ALB"],
    "form_taker_email": "mail@mail2.com",
    "form_taker_phone_number": "0689585985",
    "is_form_taker_in_family_tree": false,
    "form_taker_student_relation": "Prind",
    "student_name": "Akil",
    "father_name": "Oresti",
    "student_surname": "Rajdho",
    "student_nid": "I70908051K",
    "student_gender": "M",
    "student_birthdate": "1987-09-08",
    "student_birthplace": "Kavajë, Durrës",
    "student_citizenships": ["ALB"],
    "street_id": "89848e02-de50-493c-a0f1-aeaae8c0a802",
    "school_id": "ae66ea11-8021-4919-bcd6-6e5a56d65d62",
    "certificate_of_kindergarten": "File",
    "certificate_of_vaccination": "File",
    "certificate_of_residence": "File",
    "minority_id": "0ff853be-dc9d-4120-90fd-0dd6ea985e02",
    "court_id": "edcdcdb6-5e07-47f0-8cbd-70d90727fe8a"
}
```

::: Tip Success
Response
:::

```json
{
    "message": "Nxënësi u regjistrua me sukses!",
    "status": 200,
    "data": [
        {
            "tracking_number": "4be299b7-119a-4959-bd7e-b231b7549cdf"
        }
    ]
}
```

::: Tip Error
Response
:::

```json
{
    "message": "Fusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nThe form taker email field is required when form taker phone number is not present.\nThe form taker phone number field is required when form taker email is not present.\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\nFusha është e kërkuar!\n",
    "status": 2,
    "data": [
        {
            "form_taker_name": ["Fusha është e kërkuar!"],
            "form_taker_surname": ["Fusha është e kërkuar!"],
            "form_taker_nid": ["Fusha është e kërkuar!"],
            "form_taker_gender": ["Fusha është e kërkuar!"],
            "form_taker_birthdate": ["Fusha është e kërkuar!"],
            "form_taker_citizenships": ["Fusha është e kërkuar!"],
            "form_taker_email": [
                "The form taker email field is required when form taker phone number is not present."
            ],
            "form_taker_phone_number": [
                "The form taker phone number field is required when form taker email is not present."
            ],
            "is_form_taker_in_family_tree": ["Fusha është e kërkuar!"],
            "student_name": ["Fusha është e kërkuar!"],
            "father_name": ["Fusha është e kërkuar!"],
            "student_surname": ["Fusha është e kërkuar!"],
            "student_nid": ["Fusha është e kërkuar!"],
            "student_gender": ["Fusha është e kërkuar!"],
            "student_birthdate": ["Fusha është e kërkuar!"],
            "student_citizenships": ["Fusha është e kërkuar!"],
            "street_id": ["Fusha është e kërkuar!"],
            "school_id": ["Fusha është e kërkuar!"],
            "vaccination_medical_center_id": ["Fusha është e kërkuar!"],
            "kindergarten_id": ["Fusha është e kërkuar!"],
            "civil_state_office_id": ["Fusha është e kërkuar!"]
        }
    ]
}
```

## 4. Track Student Application

-   Request Number: 4
-   Request Type: `GET`
-   URL: `/api/webservices/track/${TRACKING_NUMBER}`

::: Tip Success
Request
:::

URL : `/api/webservices/track/485d3bec-6b08-4358-bc52-cedcbccd89e3`'

::: Tip Success
Response
:::

```json
{
    "message": "Sukses",
    "status": 200,
    "data": [
        {
            "full_name": "Kristian Rajdho",
            "school_name": "Flamuri Vlore (Zyra Vendore Arsimore Vlorë-Himarë)",
            "status": "Në pritje dokumentash"
        }
    ]
}
```
